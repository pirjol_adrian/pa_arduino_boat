//Library
#include <Adafruit_SSD1306.h>
#include <splash.h>
#include <Servo.h> 
#include <OneWire.h>
#include <DallasTemperature.h>
#include <ezButton.h>

// Define Input Connections
#define CH1 25
#define CH2 27
#define CH3 24
#define CH4 29
#define CH5 23
#define CH6 31
#define tempESC 36
#define tempApa 38
#define sensorIR 40
#define nivelApa 20
#define ledStgDrpt 11
#define ledSpateStg 13
#define ledSpateDreapta 12


// Define output Connections
#define CuvaDreapta 48
#define Carma 19
#define Acceleratie 52
#define Acceleratie2 4
#define CuvaStanga 50
#define light 42
#define laser 44
#define releu3 34
#define releu2 32
#define releu1 30
#define releu4 28
#define fun 21
#define capac 10
#define buzz 26


// Integers to represent values from sticks and pots
int ch1Value;
int ch2Value;
int ch3Value;
int ch4Value; 
int ch5Value;

// Boolean to represent switch value
bool ch6Value;

//Instante Objects
Servo myservo; 
Servo myservo1;
Servo myservo2;
Servo myservo3;
Servo myservo4;
OneWire oneWire(tempESC);

//Variable declare
int valAcceleratie; 
int valCuva;  
int valCarma; 
int valLight;
bool valLaser = false;
float valTempApa;

int valSensorIR;
volatile unsigned long pulseStart;  // timpul la care începe un puls
volatile unsigned long pulseDuration;  // durata unui puls
volatile unsigned int RPM;  // numărul de rotații pe minut

float valTempESC;
int valReleu1;
int valReleu2;
int valReleu3;
int valReleu4;
int valCapac;

DallasTemperature sensorsTempESC(&oneWire);


// Read the number of a specified channel and convert to the range provided.
// If the channel is off, return the default value
int readChannel(int channelInput, int minLimit, int maxLimit, int defaultValue){
  int ch = pulseIn(channelInput, HIGH, 30000);
  if (ch < 100) return defaultValue;
  return map(ch, 1000, 2000, minLimit, maxLimit);
}

// Read the switch channel and return a boolean value
bool readSwitch(byte channelInput, bool defaultValue){
  int intDefaultValue = (defaultValue)? 100: 0;
  int ch = readChannel(channelInput, 0, 100, intDefaultValue);
  return (ch > 50);
}

void pulseCount() {
  pulseStart = micros();  // salvează timpul la care începe un puls
}

void setup(){
 myservo1.attach(Acceleratie); 
 myservo4.attach(Acceleratie2);
 myservo2.attach(CuvaDreapta); 
 myservo3.attach(CuvaStanga); 
 myservo.attach(Carma); 
 pinMode(laser, OUTPUT);
 pinMode(tempApa, INPUT);
 pinMode(tempESC, INPUT);
 pinMode(releu1, OUTPUT);
 pinMode(releu2, OUTPUT);
 pinMode(releu3, OUTPUT);
 pinMode(releu4, OUTPUT);
 pinMode(fun, OUTPUT);
 pinMode(capac, INPUT);
 pinMode(sensorIR, INPUT);// setarea pinului senzorului IR ca pin de intrare
 attachInterrupt(digitalPinToInterrupt(sensorIR), pulseCount, RISING);  // declanșează funcția pulseCount la fiecare înălțare a semnalului de la senzorul IR



 sensorsTempESC.begin();




  // Set up serial monitor
  Serial.begin(115200);
  // Set all pins as inputs
  pinMode(CH1, INPUT);
  pinMode(CH2, INPUT);
  pinMode(CH3, INPUT);
  pinMode(CH4, INPUT);
  pinMode(CH5, INPUT);
  pinMode(CH6, INPUT);
}


void loop() {

// Get values for each channel
  ch1Value = readChannel(CH1, -100, 100, 0);
  ch2Value = readChannel(CH2, -100, 100, 0);
  ch3Value = readChannel(CH3, -100, 100, -100);
  ch4Value = readChannel(CH4, -100, 100, 0);
  ch5Value = readChannel(CH5, -100, 100, 0);
  ch6Value = readSwitch(CH6, false);
  digitalWrite(releu4, HIGH);
  sensorsTempESC.requestTemperatures(); 

//.....................LASER............................
  if(ch6Value == 1){
    valLaser = true;
  }
  if(ch6Value == 0){
    valLaser = false;
  }
  if(valLaser == true){
    digitalWrite(laser, HIGH);
    digitalWrite(releu1, LOW);
  }
  if(valLaser == false){
    digitalWrite(laser, LOW);
    digitalWrite(releu1, HIGH);
  }
//....................FINAL LASER.......................

//....................TEST..............................
  myservo4.write(50);
  digitalWrite(ledStgDrpt, HIGH);
  digitalWrite(ledSpateDreapta, HIGH);
  digitalWrite(ledSpateStg, HIGH);
  analogWrite(fun, 255);
  valSensorIR = digitalRead(sensorIR);

  float temp = analogRead(tempApa);
  valTempApa = temp;
  
  float temp1 = sensorsTempESC.getTempCByIndex(1);
  valTempESC = temp1;


  valLight = digitalRead(light);
  valCapac = digitalRead(capac);

//.............FINAL TEST..............................

//.............RPM.....................................
  pulseDuration = pulseIn(sensorIR, LOW);  // măsoară durata pulsului LOW de pe pinul senzorului IR
  if (pulseDuration != 0) {  // verificăm dacă pulsul a fost detectat
    RPM = 60000000 / pulseDuration;  // calculează numărul de rotații pe minut
    
  }else{
    RPM=0;
  }
//.............FINAL RPM.............................. 




//...................CUVE............................
  valCuva = map(ch5Value, -0, 100, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
  if(ch5Value > 25){
      myservo2.write(50);  
  }
  if(ch5Value < 0){
      
      myservo3.write(50);      
  }
  if(ch5Value < 25 && ch5Value > -25){
  myservo2.write(0); 
  myservo3.write(0); 
  }
//....................Final CUVE........................

  valCarma = map(ch4Value, -100, 100, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
  myservo.write(valCarma);                        // sets the servo position according to the scaled value

  valAcceleratie = map(ch3Value, -100, 100, 0, 180);     // scale it to use it with the servo (value between 0 and 180)
  myservo1.write(valAcceleratie);
  myservo4.write(valAcceleratie);              // sets the servo position according to the scaled value
  
  // Print to Serial Monitor
    Serial.print("Ch1: ");
  Serial.print(ch1Value);
    Serial.print(" | Ch2: ");
  Serial.print(ch2Value);
    Serial.print(" | Ch3: ");
  Serial.print(ch3Value);
    Serial.print(" | Ch4: ");
  Serial.print(ch4Value);
    Serial.print(" | Ch5: ");
  Serial.print(ch5Value);
    Serial.print(" | Ch6: ");
  Serial.print(ch6Value);
    Serial.print(" | Acc: ");
  Serial.print(valAcceleratie);
    Serial.print(" | Carma: ");
  Serial.print(valCarma);
    Serial.print(" | light: ");
  Serial.print(valLight);
    Serial.print(" | Laser: ");
  Serial.print(valLaser);
    Serial.print(" | Apa: ");
  Serial.print(valTempApa);
  Serial.print(" C");
   Serial.print(" | RPM: ");
  Serial.print(RPM);
    Serial.print(" | ESC: ");
  Serial.print(valTempESC);
  Serial.print(" C");
   Serial.print(" | Releu1: ");
  Serial.print(valReleu1);
   Serial.print(" | Releu2: ");
  Serial.print(valReleu2);
   Serial.print(" | Releu3: ");
  Serial.print(valReleu3);
   Serial.print(" | Releu4: ");
  Serial.print(valReleu4);
     Serial.print(" | Capac: ");
  Serial.println(valCapac);

  
 

}
